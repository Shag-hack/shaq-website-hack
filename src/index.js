import React from 'react';
import ReactDOM from 'react-dom';
import Shag from './Shag';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import 'semantic-ui-css/semantic.min.css';


ReactDOM.render(<Shag />, document.getElementById('app'));
registerServiceWorker();
