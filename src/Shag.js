import React, { Component } from 'react';

class Shag extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        person: {firstName: "",
                lastName: "",
                cellphone:"",   
                idNumber:"",
                image:"",
                isIdentical:"",
                confidence:""
            },
        simulationTimer:setTimeout(this.simulateDataArrived.bind(this), 100)
        };
    };
    
    _onNewDataArrived(data){
        this.setState({person:data})
        
    };

    simulateDataArrived(){
        clearTimeout(this.state.simulationTimer);
        let data = {
                firstName: "yolo",
                lastName: "bla",
                cellphone:"05454483744",   
                idNumber:123456789,
                matched_image:"./booga.jpg",
                original_image:"./original_booga.jpg",
                isIdentical:"true",
                confidence:0.9
            };
        this._onNewDataArrived(data);
    };

    render(){
        let person = this.state.person;
        return<div className="ui three column grid">
                <div className="column" id="originial-image">
                    <img className="ui small image" src= {person.matched_image}/>
                </div>
                <div className="column" id="confidence">
                    confidence: {person.confidence}
                </div>
                <div className="column" id="current-image">
                    <img className="ui small image" src= {person.matched_image}/>
                </div>
            <div className="centered aligned row">
                <div className="column" id="info">
                    <div>First Name: {person.firstName}</div>
                    <div>Last Name: {person.lastName}</div>
                    <div>cellphone: {person.cellphone}</div>
                    <div>idNumber: {person.idNumber}</div>
                </div>
            </div>
        </div>
    }
}

export default Shag